EESchema Schematic File Version 4
LIBS:DP_spybrd-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2900 2150 2900 2750
Wire Wire Line
	3000 2150 3000 2750
Wire Wire Line
	3100 2150 3100 2750
Wire Wire Line
	3200 2150 3200 2750
Wire Wire Line
	3300 2150 3300 2750
Wire Wire Line
	3400 2150 3400 2750
Wire Wire Line
	3500 2150 3500 2750
Wire Wire Line
	3600 2150 3600 2750
Wire Wire Line
	3700 2150 3700 2750
Wire Wire Line
	3800 2150 3800 2750
Wire Wire Line
	3900 2150 3900 2750
Wire Wire Line
	4000 2150 4000 2750
Wire Wire Line
	4100 2150 4100 2750
Wire Wire Line
	4200 2150 4200 2750
Wire Wire Line
	4300 2150 4300 2750
Wire Wire Line
	4400 2150 4400 2750
Wire Wire Line
	4500 2150 4500 2750
Wire Wire Line
	4600 2150 4600 2750
Wire Wire Line
	4700 2150 4700 2750
Wire Wire Line
	4800 2150 4800 2750
Wire Wire Line
	5150 1500 5600 1500
Text Label 2900 2750 1    50   ~ 0
DP1_SIG_0_N
Text Label 3000 2750 1    50   ~ 0
GND
Text Label 3100 2750 1    50   ~ 0
DP1_SIG_0_P
Text Label 3200 2750 1    50   ~ 0
DP1_SIG_1_N
Text Label 3300 2750 1    50   ~ 0
GND
Text Label 3400 2750 1    50   ~ 0
DP1_SIG_1_P
Text Label 3500 2750 1    50   ~ 0
DP1_SIG_2_N
Text Label 3600 2750 1    50   ~ 0
GND
Text Label 3700 2750 1    50   ~ 0
DP1_SIG_2_P
Text Label 3800 2750 1    50   ~ 0
DP1_SIG_3_N
Text Label 3900 2750 1    50   ~ 0
GND
Text Label 4000 2750 1    50   ~ 0
DP1_SIG_3_P
Text Label 4100 2750 1    50   ~ 0
DP1_SIG_4
Text Label 4200 2750 1    50   ~ 0
DP1_SIG_5
Text Label 4300 2750 1    50   ~ 0
DP1_SIG_6_N
Text Label 4400 2750 1    50   ~ 0
GND
Text Label 4500 2750 1    50   ~ 0
DP1_SIG_6_P
Text Label 4600 2750 1    50   ~ 0
DP1_AUX_1
Text Label 4700 2750 1    50   ~ 0
DP1_AUX_2
Text Label 4800 2750 1    50   ~ 0
DP1_AUX_3
Text Label 5600 1500 2    50   ~ 0
DP1_SGND_1
Wire Wire Line
	2850 5350 2850 4750
Wire Wire Line
	2950 5350 2950 4750
Wire Wire Line
	3050 5350 3050 4750
Wire Wire Line
	3150 5350 3150 4750
Wire Wire Line
	3250 5350 3250 4750
Wire Wire Line
	3350 5350 3350 4750
Wire Wire Line
	3450 5350 3450 4750
Wire Wire Line
	3550 5350 3550 4750
Wire Wire Line
	3650 5350 3650 4750
Wire Wire Line
	3750 5350 3750 4750
Wire Wire Line
	3850 5350 3850 4750
Wire Wire Line
	3950 5350 3950 4750
Wire Wire Line
	4050 5350 4050 4750
Wire Wire Line
	4150 5350 4150 4750
Wire Wire Line
	4250 5350 4250 4750
Wire Wire Line
	4350 5350 4350 4750
Wire Wire Line
	4450 5350 4450 4750
Wire Wire Line
	4550 5350 4550 4750
Wire Wire Line
	4650 5350 4650 4750
Wire Wire Line
	4750 5350 4750 4750
Wire Wire Line
	5100 6000 5550 6000
Text Label 2850 4750 3    50   ~ 0
DP1_SIG_0_N
Text Label 2950 4750 3    50   ~ 0
GND
Text Label 3050 4750 3    50   ~ 0
DP1_SIG_0_P
Text Label 3150 4750 3    50   ~ 0
DP1_SIG_1_N
Text Label 3250 4750 3    50   ~ 0
GND
Text Label 3350 4750 3    50   ~ 0
DP1_SIG_1_P
Text Label 3450 4750 3    50   ~ 0
DP1_SIG_2_N
Text Label 3550 4750 3    50   ~ 0
GND
Text Label 3650 4750 3    50   ~ 0
DP1_SIG_2_P
Text Label 3750 4750 3    50   ~ 0
DP1_SIG_3_N
Text Label 3850 4750 3    50   ~ 0
GND
Text Label 3950 4750 3    50   ~ 0
DP1_SIG_3_P
Text Label 4050 4750 3    50   ~ 0
DP1_SIG_4
Text Label 4150 4750 3    50   ~ 0
DP1_SIG_5
Text Label 4250 4750 3    50   ~ 0
DP1_SIG_6_N
Text Label 4350 4750 3    50   ~ 0
GND
Text Label 4450 4750 3    50   ~ 0
DP1_SIG_6_P
Text Label 4550 4750 3    50   ~ 0
DP1_AUX_1
Text Label 4650 4750 3    50   ~ 0
DP1_AUX_2
Text Label 4750 4750 3    50   ~ 0
DP1_AUX_3
Text Label 5550 6000 2    50   ~ 0
DP1_SGND_1
Wire Wire Line
	3050 3550 3050 3750
Wire Wire Line
	3150 3550 3150 3750
Wire Wire Line
	3250 3550 3250 3750
Wire Wire Line
	3350 3550 3350 3750
Wire Wire Line
	3450 3550 3450 3750
Wire Wire Line
	3550 3550 3550 3750
Wire Wire Line
	3650 3550 3650 3750
Wire Wire Line
	3750 3550 3750 3750
Wire Wire Line
	3850 3550 3850 3750
Wire Wire Line
	3950 3550 3950 3750
Wire Wire Line
	4050 3550 4050 3750
Wire Wire Line
	4150 3550 4150 3750
Wire Wire Line
	4250 3550 4250 3750
Wire Wire Line
	4350 3550 4350 3750
Wire Wire Line
	4450 3550 4450 3750
Text Label 3050 3750 3    50   ~ 0
DP1_SIG_0_N
Text Label 3150 3750 3    50   ~ 0
DP1_SIG_0_P
Text Label 3250 3750 3    50   ~ 0
DP1_SIG_1_N
Text Label 3350 3750 3    50   ~ 0
DP1_SIG_1_P
Text Label 3450 3750 3    50   ~ 0
DP1_SIG_2_N
Text Label 3550 3750 3    50   ~ 0
DP1_SIG_2_P
Text Label 3650 3750 3    50   ~ 0
DP1_SIG_3_N
Text Label 3750 3750 3    50   ~ 0
DP1_SIG_3_P
Text Label 3850 3750 3    50   ~ 0
DP1_SIG_4
Text Label 3950 3750 3    50   ~ 0
DP1_SIG_5
Text Label 4050 3750 3    50   ~ 0
DP1_SIG_6_N
Text Label 4150 3750 3    50   ~ 0
DP1_SIG_6_P
Text Label 4250 3750 3    50   ~ 0
DP1_AUX_1
Text Label 4350 3750 3    50   ~ 0
DP1_AUX_2
Text Label 4450 3750 3    50   ~ 0
DP1_AUX_3
$Comp
L Device:R R1
U 1 1 5EA9BC83
P 6500 2350
F 0 "R1" V 6293 2350 50  0000 C CNN
F 1 "100" V 6384 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6430 2350 50  0001 C CNN
F 3 "~" H 6500 2350 50  0001 C CNN
	1    6500 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 2350 6250 2350
Wire Wire Line
	6650 2350 6750 2350
$Comp
L Device:R R2
U 1 1 5EAAF953
P 6500 2700
F 0 "R2" V 6293 2700 50  0000 C CNN
F 1 "100" V 6384 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6430 2700 50  0001 C CNN
F 3 "~" H 6500 2700 50  0001 C CNN
	1    6500 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 2700 6250 2700
Wire Wire Line
	6650 2700 6750 2700
$Comp
L Device:R R3
U 1 1 5EAB2DFF
P 6500 3050
F 0 "R3" V 6293 3050 50  0000 C CNN
F 1 "100" V 6384 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6430 3050 50  0001 C CNN
F 3 "~" H 6500 3050 50  0001 C CNN
	1    6500 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 3050 6250 3050
Wire Wire Line
	6650 3050 6750 3050
$Comp
L Device:R R4
U 1 1 5EAB6209
P 6500 3400
F 0 "R4" V 6293 3400 50  0000 C CNN
F 1 "100" V 6384 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6430 3400 50  0001 C CNN
F 3 "~" H 6500 3400 50  0001 C CNN
	1    6500 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 3400 6250 3400
Wire Wire Line
	6650 3400 6750 3400
$Comp
L Device:R R5
U 1 1 5EAB992F
P 6500 3750
F 0 "R5" V 6293 3750 50  0000 C CNN
F 1 "100" V 6384 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6430 3750 50  0001 C CNN
F 3 "~" H 6500 3750 50  0001 C CNN
	1    6500 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 3750 6250 3750
Text Label 6250 2350 2    50   ~ 0
DP1_SIG_0_P
Text Label 6750 2350 0    50   ~ 0
DP1_SIG_0_N
Text Label 6250 2700 2    50   ~ 0
DP1_SIG_1_P
Text Label 6750 2700 0    50   ~ 0
DP1_SIG_1_N
Text Label 6250 3050 2    50   ~ 0
DP1_SIG_2_P
Text Label 6750 3050 0    50   ~ 0
DP1_SIG_2_N
Text Label 6250 3400 2    50   ~ 0
DP1_SIG_3_P
Text Label 6750 3400 0    50   ~ 0
DP1_SIG_3_N
Text Label 6250 3750 2    50   ~ 0
DP1_SIG_6_P
Wire Wire Line
	6650 3750 6850 3750
Text Label 6850 3750 0    50   ~ 0
DP1_SIG_6_N
Wire Wire Line
	6300 5250 6300 5450
Wire Wire Line
	6200 5250 6200 5450
Wire Wire Line
	6400 5250 6400 5450
Wire Wire Line
	6500 5250 6500 5450
Wire Wire Line
	6600 5250 6600 5450
Text Label 6400 5450 3    50   ~ 0
GND
Text Label 6500 5450 3    50   ~ 0
GND
Text Label 6600 5450 3    50   ~ 0
GND
$Comp
L DP_spybrd-rescue:DISPLAY_PORT DP2
U 1 1 5EA0CCF1
P 3800 1300
F 0 "DP2" V 4343 1142 60  0000 C CNN
F 1 "DISPLAY_PORT" V 4237 1142 60  0000 C CNN
F 2 "Display port:DP1RD20JQ1R400" V 4131 1142 60  0001 C CNN
F 3 "" H 3750 1300 60  0000 C CNN
	1    3800 1300
	0    -1   -1   0   
$EndComp
$Comp
L DP_spybrd-rescue:DISPLAY_PORT DP1
U 1 1 5EA12481
P 3750 6200
F 0 "DP1" V 3336 7278 60  0000 L CNN
F 1 "DISPLAY_PORT" V 3442 7278 60  0000 L CNN
F 2 "Display port:DP1RD20JQ1R400" V 3548 7278 60  0001 L CNN
F 3 "" H 3700 6200 60  0000 C CNN
	1    3750 6200
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 5EA3A3D2
P 6400 5050
F 0 "J2" V 6338 4762 50  0000 R CNN
F 1 "Conn_01x05_Female" V 6247 4762 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 6400 5050 50  0001 C CNN
F 3 "~" H 6400 5050 50  0001 C CNN
	1    6400 5050
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x15_Female J1
U 1 1 5EA1DA99
P 3750 3350
F 0 "J1" V 3915 3330 50  0000 C CNN
F 1 "Conn_01x15_Female" V 3824 3330 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 3750 3350 50  0001 C CNN
F 3 "~" H 3750 3350 50  0001 C CNN
	1    3750 3350
	0    -1   -1   0   
$EndComp
Text Label 6200 5450 3    50   ~ 0
DP1_SGND_1
Text Label 6300 5450 3    50   ~ 0
GND
$Comp
L Device:R R6
U 1 1 5EAA6628
P 6100 4150
F 0 "R6" V 5893 4150 50  0000 C CNN
F 1 "100" V 5984 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6030 4150 50  0001 C CNN
F 3 "~" H 6100 4150 50  0001 C CNN
	1    6100 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	6250 4150 6450 4150
$Comp
L Device:L L1
U 1 1 5EAAC1A7
P 6600 4150
F 0 "L1" V 6419 4150 50  0000 C CNN
F 1 "100n" V 6510 4150 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 6600 4150 50  0001 C CNN
F 3 "~" H 6600 4150 50  0001 C CNN
	1    6600 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 4150 6850 4150
Wire Wire Line
	5950 4150 5800 4150
Wire Wire Line
	5700 4450 5700 4150
Wire Wire Line
	6900 4450 6900 4150
$Comp
L Connector:Conn_01x03_Female J3
U 1 1 5EABD554
P 6400 4800
F 0 "J3" V 6246 4948 50  0000 L CNN
F 1 "Conn_01x03_Female" V 6337 4948 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6400 4800 50  0001 C CNN
F 3 "~" H 6400 4800 50  0001 C CNN
	1    6400 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	5700 4450 6300 4450
Wire Wire Line
	6300 4450 6300 4600
Wire Wire Line
	6900 4450 6400 4450
Wire Wire Line
	6400 4450 6400 4600
Wire Wire Line
	6500 4600 6750 4600
Text Label 6750 4600 0    50   ~ 0
GND
$Comp
L Jumper:SolderJumper_2_Bridged JP1
U 1 1 5EAB80AD
P 5550 4050
F 0 "JP1" H 5550 4255 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 5550 4164 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 5550 4050 50  0001 C CNN
F 3 "~" H 5550 4050 50  0001 C CNN
	1    5550 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 4000 6850 4150
Connection ~ 6850 4150
Wire Wire Line
	6850 4150 6900 4150
Wire Wire Line
	5700 4050 5800 4050
Wire Wire Line
	5800 4050 5800 4150
Connection ~ 5800 4150
Wire Wire Line
	5800 4150 5700 4150
Wire Wire Line
	7000 4000 6850 4000
NoConn ~ 7300 4000
NoConn ~ 5400 4050
NoConn ~ 4800 1550
$Comp
L Jumper:SolderJumper_2_Bridged JP3
U 1 1 5EB7291B
P 8300 4350
F 0 "JP3" H 8300 4555 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 8300 4464 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 8300 4350 50  0001 C CNN
F 3 "~" H 8300 4350 50  0001 C CNN
	1    8300 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 4350 8650 4350
Text Label 8650 4350 0    50   ~ 0
GND
NoConn ~ 8150 4350
$Comp
L Jumper:SolderJumper_2_Bridged JP2
U 1 1 5EABAD28
P 7150 4000
F 0 "JP2" H 7150 4205 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 7150 4114 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_Pad1.0x1.5mm" H 7150 4000 50  0001 C CNN
F 3 "~" H 7150 4000 50  0001 C CNN
	1    7150 4000
	-1   0    0    1   
$EndComp
$EndSCHEMATC
